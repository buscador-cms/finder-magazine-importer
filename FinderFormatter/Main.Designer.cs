﻿namespace FinderFormatter
{
    partial class FinderFormatter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FinderFormatter));
            this.FolderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.ModelSelector = new System.Windows.Forms.OpenFileDialog();
            this.parsed = new System.Windows.Forms.TextBox();
            this.selectFolderBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.selectedFolder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.startBtn = new System.Windows.Forms.Button();
            this.selectModelBtn = new System.Windows.Forms.Button();
            this.selectedModelFile = new System.Windows.Forms.TextBox();
            this.TopMenu = new System.Windows.Forms.ToolStrip();
            this.saveMessages = new System.Windows.Forms.ToolStripButton();
            this.fixFolderNames = new System.Windows.Forms.ToolStripButton();
            this.exportSQL = new System.Windows.Forms.ToolStripLabel();
            this.SaveLog = new System.Windows.Forms.SaveFileDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.countPagesBtn = new System.Windows.Forms.Button();
            this.renameFiles = new System.Windows.Forms.Button();
            this.TopMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // FolderBrowser
            // 
            this.FolderBrowser.SelectedPath = "Nueva carpeta";
            // 
            // ModelSelector
            // 
            this.ModelSelector.DefaultExt = "csv";
            this.ModelSelector.FileName = "pdfs.csv";
            this.ModelSelector.Filter = "Archivo CSV (*.csv)|*.csv";
            this.ModelSelector.Title = "Seleccionar el modelo a utilizar";
            // 
            // parsed
            // 
            this.parsed.Location = new System.Drawing.Point(12, 127);
            this.parsed.Multiline = true;
            this.parsed.Name = "parsed";
            this.parsed.ReadOnly = true;
            this.parsed.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.parsed.Size = new System.Drawing.Size(784, 341);
            this.parsed.TabIndex = 4;
            // 
            // selectFolderBtn
            // 
            this.selectFolderBtn.Location = new System.Drawing.Point(395, 51);
            this.selectFolderBtn.Name = "selectFolderBtn";
            this.selectFolderBtn.Size = new System.Drawing.Size(116, 23);
            this.selectFolderBtn.TabIndex = 0;
            this.selectFolderBtn.Text = "Elegir Carpeta";
            this.selectFolderBtn.UseVisualStyleBackColor = true;
            this.selectFolderBtn.Click += new System.EventHandler(this.selectFolderBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Carpeta:";
            // 
            // selectedFolder
            // 
            this.selectedFolder.Location = new System.Drawing.Point(65, 53);
            this.selectedFolder.Name = "selectedFolder";
            this.selectedFolder.Size = new System.Drawing.Size(324, 20);
            this.selectedFolder.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Modelo:";
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(645, 83);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(75, 23);
            this.startBtn.TabIndex = 6;
            this.startBtn.Text = "Iniciar";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // selectModelBtn
            // 
            this.selectModelBtn.Location = new System.Drawing.Point(395, 81);
            this.selectModelBtn.Name = "selectModelBtn";
            this.selectModelBtn.Size = new System.Drawing.Size(116, 23);
            this.selectModelBtn.TabIndex = 7;
            this.selectModelBtn.Text = "Elegir Archivo";
            this.selectModelBtn.UseVisualStyleBackColor = true;
            this.selectModelBtn.Click += new System.EventHandler(this.selectModelBtn_Click);
            // 
            // selectedModelFile
            // 
            this.selectedModelFile.Location = new System.Drawing.Point(65, 83);
            this.selectedModelFile.Name = "selectedModelFile";
            this.selectedModelFile.Size = new System.Drawing.Size(324, 20);
            this.selectedModelFile.TabIndex = 8;
            // 
            // TopMenu
            // 
            this.TopMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveMessages,
            this.fixFolderNames,
            this.exportSQL});
            this.TopMenu.Location = new System.Drawing.Point(0, 0);
            this.TopMenu.Name = "TopMenu";
            this.TopMenu.Size = new System.Drawing.Size(808, 25);
            this.TopMenu.TabIndex = 10;
            this.TopMenu.Text = "toolStrip1";
            // 
            // saveMessages
            // 
            this.saveMessages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveMessages.Image = ((System.Drawing.Image)(resources.GetObject("saveMessages.Image")));
            this.saveMessages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveMessages.Name = "saveMessages";
            this.saveMessages.Size = new System.Drawing.Size(23, 22);
            this.saveMessages.Text = "Guardar los Mensajes en un Archivo";
            this.saveMessages.Click += new System.EventHandler(this.saveMessages_Click);
            // 
            // fixFolderNames
            // 
            this.fixFolderNames.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fixFolderNames.Image = ((System.Drawing.Image)(resources.GetObject("fixFolderNames.Image")));
            this.fixFolderNames.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fixFolderNames.Name = "fixFolderNames";
            this.fixFolderNames.Size = new System.Drawing.Size(23, 22);
            this.fixFolderNames.Text = "Reparar Nombres de Carpetas";
            this.fixFolderNames.Click += new System.EventHandler(this.fixFolderNames_Click);
            // 
            // exportSQL
            // 
            this.exportSQL.Name = "exportSQL";
            this.exportSQL.Size = new System.Drawing.Size(28, 22);
            this.exportSQL.Text = "SQL";
            this.exportSQL.Click += new System.EventHandler(this.exportSQL_Click);
            // 
            // SaveLog
            // 
            this.SaveLog.DefaultExt = "txt";
            this.SaveLog.FileName = "FinderImporter";
            this.SaveLog.Filter = "Documento de Texto (*.txt)|*.txt";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Mensajes:";
            // 
            // countPagesBtn
            // 
            this.countPagesBtn.Location = new System.Drawing.Point(517, 81);
            this.countPagesBtn.Name = "countPagesBtn";
            this.countPagesBtn.Size = new System.Drawing.Size(122, 23);
            this.countPagesBtn.TabIndex = 12;
            this.countPagesBtn.Text = "Contar Páginas";
            this.countPagesBtn.UseVisualStyleBackColor = true;
            this.countPagesBtn.Click += new System.EventHandler(this.countPagesBtn_Click);
            // 
            // renameFiles
            // 
            this.renameFiles.Location = new System.Drawing.Point(518, 52);
            this.renameFiles.Name = "renameFiles";
            this.renameFiles.Size = new System.Drawing.Size(121, 23);
            this.renameFiles.TabIndex = 13;
            this.renameFiles.Text = "Renombrar Archivos";
            this.renameFiles.UseVisualStyleBackColor = true;
            this.renameFiles.Click += new System.EventHandler(this.renameFiles_Click);
            // 
            // FinderFormatter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 480);
            this.Controls.Add(this.renameFiles);
            this.Controls.Add(this.countPagesBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TopMenu);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.selectedModelFile);
            this.Controls.Add(this.selectModelBtn);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.parsed);
            this.Controls.Add(this.selectedFolder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selectFolderBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(824, 519);
            this.MinimumSize = new System.Drawing.Size(824, 519);
            this.Name = "FinderFormatter";
            this.Text = "Finder Formatter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FinderFormatter_FormClosing);
            this.Load += new System.EventHandler(this.FinderFormatter_Load);
            this.TopMenu.ResumeLayout(false);
            this.TopMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog FolderBrowser;
        private System.Windows.Forms.OpenFileDialog ModelSelector;
        private System.Windows.Forms.TextBox parsed;
        private System.Windows.Forms.Button selectFolderBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox selectedFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Button selectModelBtn;
        private System.Windows.Forms.TextBox selectedModelFile;
        private System.Windows.Forms.ToolStrip TopMenu;
        private System.Windows.Forms.ToolStripButton fixFolderNames;
        private System.Windows.Forms.ToolStripButton saveMessages;
        private System.Windows.Forms.SaveFileDialog SaveLog;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripLabel exportSQL;
        private System.Windows.Forms.Button countPagesBtn;
        private System.Windows.Forms.Button renameFiles;
    }
}

