﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf;

namespace FinderFormatter
{

    public partial class FinderFormatter : Form
    {
        private PdfReader Pdf;
        private StringBuilder magSql;
        private StringBuilder artSql;
        private StringBuilder autSql;
        private StringBuilder artSqlUpdate;
        private string getMagIDFormat = "(SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = {1} AND PublishYear = {0})";
        private string authorFormat = "";

        public FinderFormatter()
        {
            InitializeComponent();
            authorFormat = "EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '{0}', @AuthorFName = '{1}', @AuthorMName = '{2}', @AuthorID = @ID OUTPUT;\n" +
                "SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '{3}');\n" +
                "IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)\nBEGIN\nINSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);\nEND\n\n";
            ResetSqlBuilders();
            artSqlUpdate = new StringBuilder();
        }

        private void selectFolderBtn_Click(object sender, EventArgs e)
        {
            if (FolderBrowser.ShowDialog() == DialogResult.OK)
            {
                selectedFolder.Text = FolderBrowser.SelectedPath;
            }
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            parsed.Text = "";

            if (MakeBaseFolder())
            {
                ParseFile();
            }
        }

        private bool MakeBaseFolder()
        {
            string magFolder = Path.Combine(selectedFolder.Text, "Magazine");

            try
            {
                if (!Directory.Exists(magFolder))
                {
                    //Directory.Delete(magFolder, true);

                    Directory.CreateDirectory(magFolder);
                    return Directory.Exists(magFolder);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(
                        String.Format("Error al intentar crear la nueva carpeta: {0}", e.Message));
            }

            return true;
        }

        private void selectModelBtn_Click(object sender, EventArgs e)
        {
            if (ModelSelector.ShowDialog() == DialogResult.OK)
            {
                selectedModelFile.Text = ModelSelector.FileName;
            }
        }

        private void ResetSqlBuilders()
        {

            if (magSql == null)
            {
                magSql = new StringBuilder();
            }
            else
            {
                magSql.Clear();
            }

            if (artSql == null)
            {
                artSql = new StringBuilder();
            }
            else
            {
                artSql.Clear();
            }

            if (autSql == null)
            {
                autSql = new StringBuilder();
            }
            else
            {
                autSql.Clear();
            }
        }

        private void ParseFile()
        {
            ResetSqlBuilders();
            string file = selectedModelFile.Text;
            string userFolder = selectedFolder.Text;
            string baseFolder = Path.Combine(userFolder, "Magazine");
            List<string> parsedFolders = new List<string>();

            if (file.Length > 0)
            {
                try
                {
                    string[] lines = File.ReadAllLines(file, Encoding.UTF8);
                    // lineParts[0] = year, lineParts[1] = fileName, lineParts[2] = Title, lineParts[3] = Authors
                    string[] lineParts = null;
                    Regex yearMatch = new Regex(@"^(.+\s)*(\d{4})", RegexOptions.IgnoreCase);
                    string year = "";
                    int count = 1;
                    string fname = "";
                    long lastYear = 0;
                    int lastMagNo = 0;
                    WriteMessage("#--- START FILE PARSING ---#");

                    foreach (string line in lines)
                    {
                        if (!line.StartsWith("#"))
                        {
                            lineParts = line.Split('|');
                            bool parse = Regex.IsMatch(lineParts[1], @"^\d+_\d+(_\d+.+)?\.pdf$");
                            WriteMessage(String.Format("### LINE {1}: {0}...", line, count), false);

                            if (yearMatch.IsMatch(lineParts[0]))
                            {
                                year = "";
                                year = yearMatch.Replace(lineParts[0], "$2").Trim();
                            }

                            WriteMessage(String.Format(" YEAR {0}", year), true);

                            if (parse)
                            {
                                WriteMessage("");
                                fname = RenameFile(SanitizeFileName(lineParts[1]).Trim());
                                string[] fileParts = Path.GetFileNameWithoutExtension(fname).Split('_');
                                int volNo = int.Parse(fileParts[0].Trim());
                                int artNo = int.Parse(fileParts[1].Trim());

                                if (long.Parse(year) != lastYear || lastMagNo != volNo)
                                {
                                    magSql.AppendFormat("({0}, {1}, 'none'),\n", year, volNo);
                                    lastYear = long.Parse(year);
                                    lastMagNo = volNo;
                                }

                                string newFolder = Path.Combine(baseFolder, year, volNo.ToString(), artNo.ToString());

                                if (!parsedFolders.Contains(newFolder) && !Directory.Exists(newFolder))
                                {
                                    if (MakeStructure(newFolder))
                                    {
                                        if (MoveFiles(Path.Combine(userFolder, GetSourceFolder(fname)), newFolder))
                                        {
                                            parsedFolders.Add(newFolder);
                                        }
                                    }
                                }

                                artSql.AppendFormat(
                                                "({0}, '{1}', '{2}', '{3}', {4}, {5}),\n",
                                                artNo,
                                                fname,
                                                EscapeSingleQuotes(lineParts[2]),
                                                "",
                                                GetPageCount(Path.Combine(newFolder, fname)),
                                                String.Format(getMagIDFormat, year, volNo));

                                if (lineParts[3] != null && lineParts[3].Length > 0)
                                {
                                    AppendAuthors(EscapeSingleQuotes(lineParts[3]), fname);
                                }
                                else
                                {
                                    WriteMessage("# WARNING: NO AUTHORS! #");
                                }
                            }
                            else
                            {
                                WriteMessage(" # SKIPPED #");
                            }

                            count++;
                        }
                    }

                    WriteSQLFile();
                    WriteMessage("#--- END FILE PARSING ---#");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Error: {0}\n\n{1}", ex.Message, ex.StackTrace));
                }
            }
        }

        private int GetPageCount(string path)
        {
            int pc = 0;
            
            if (File.Exists(path))
            {
                Pdf = new PdfReader(path);
                pc = Pdf.NumberOfPages;
                Pdf.Close();
            }

            return pc;
        }

        private void AppendAuthors(string authors, string fileName)
        {
            Regex multiSpace = new Regex(@"\s{2,}");

            if (authors.Length > 0)
            {
                string[] parts = authors.Split('#');

                for(int i = 0; i < parts.Length; i++)
                {
                    parts[i] = parts[i].Trim();
                }

                // (de(l|\sla)\s[A-ZÑ][a-z]+)|[Vv]on\s[\w\W]+
                try
                {
                    WriteMessage("Appending Authors...", false);

                    foreach (string author in parts)
                    {
                        string[] name = multiSpace.Replace(author.Trim(), " ").Split(' ');

                        for (int i = 0; i < name.Length; i++)
                        {
                            if (name.Length > 0)
                            {
                                name[i] = name[i].Trim();
                                name[i] = Regex.Replace(name[i], @"_", " ");
                            }
                        }

                        switch (name.Length)
                        {
                            case 1:
                                autSql.AppendFormat(authorFormat, "", name[0], "", fileName);
                                break;

                            case 2:
                                autSql.AppendFormat(authorFormat, name[0], name[1], "", fileName);
                                break;

                            case 3:
                                autSql.AppendFormat(authorFormat, name[0], name[1], name[2], fileName);
                                break;

                            case 4:
                                autSql.AppendFormat(
                                    authorFormat,
                                    String.Format("{0} {1}", name[0], name[1]),
                                    name[2],
                                    name[3],
                                    fileName);
                                break;

                            case 5:
                                autSql.AppendFormat(
                                    authorFormat,
                                    String.Format("{0} {1} {2}", name[0], name[1], name[2]),
                                    name[3],
                                    name[4],
                                    fileName);
                                break;

                            default:
                                autSql.AppendFormat(authorFormat, "", String.Join(" ", name), "", fileName);
                                break;
                        }
                    }

                    WriteMessage(" DONE!");
                }
                catch (Exception e)
                {
                    WriteMessage("## ERROR: " + e.Message + " " + e.StackTrace);
                }
            }
        }

        private bool MakeStructure(string path)
        {
            try
            {
                Directory.CreateDirectory(path);

                return Directory.Exists(path);
            }
            catch (Exception e)
            {
                MessageBox.Show(String.Format("No se pudo crear la estructura de archivos: {0}", e.Message));
            }

            return false;
        }

        private bool MoveFiles(string source, string destination)
        {
            bool moved = true;
            WriteMessage(String.Format("MOVE_FILES: Source {0}, Destination {1}", source, destination));

            if (Directory.Exists(source))
            {
                string[] sourceFiles = Directory.GetFiles(source);
                string newPath = "";

                try
                {
                    foreach (string file in sourceFiles)
                    {
                        string fname = Path.GetFileName(file);
                        newPath = Path.Combine(destination, fname);

                        if (!File.Exists(newPath))
                        {
                            WriteMessage(String.Format("Moving file {0} to {1}...", file, newPath), false);

                            File.Copy(file, newPath);

                            if (File.Exists(newPath))
                                WriteMessage(" DONE!");
                            else
                                WriteMessage(" # FILE COPY FILED #");
                        }
                        else
                        {
                            WriteMessage(String.Format("File {0} exists -- SKIPPED\n", newPath));
                        }
                    }
                }
                catch (Exception e)
                {
                    WriteMessage("*** ERROR: " + e.Message + " ***");
                    return false;
                }
            }
            else
            {
                WriteMessage("- Source folder does not exists - nothing done.");
            }

            return moved;
        }

        private string GetSourceFolder(string fileName)
        {
            if(fileName.Length > 0) {
                int pos = fileName.LastIndexOf('_');

                if(pos > 0)
                    return fileName.Substring(0, pos);
            }

            return fileName;
        }

        private string SanitizeFileName(string fileName)
        {
            fileName = Regex.Replace(fileName, @"(_{2,}|-{1,})", "_");
            fileName = Regex.Replace(fileName, @"\s", "");

            return fileName;
        }

        private void WriteMessage(bool newLine = true)
        {
            WriteMessage("", newLine);
        }

        private void WriteMessage(string text, bool newLine = true)
        {
            parsed.AppendText(text);

            if (newLine)
                parsed.AppendText("\n");
        }

        private void ClearParsed()
        {
            parsed.Text = "";
        }

        private void FixFolderNames()
        {
            string rootFolder = selectedFolder.Text;
            parsed.Text = "";

            if (Directory.Exists(rootFolder))
            {
                try
                {
                    string[] folders = Directory.GetDirectories(rootFolder);

                    foreach (string folder in folders)
                    {
                        string folderName = GetFolderName(folder);

                        if (!Regex.IsMatch(folderName, @"^\d+_\d+.+$"))
                        {
                            string newFolder = Path.Combine(rootFolder, SanitizeFileName(SanitizeFileName(folderName)));
                            WriteMessage(String.Format(
                                "Trying to rename folder \"{0}\" to \"{1}\"...", folder, newFolder), false);
                            Directory.Move(folder, newFolder);

                            if (Directory.Exists(newFolder))
                                WriteMessage(" SUCCESS!");
                            else
                                WriteMessage(" FAILED!");

                            WriteMessage();
                        }
                    }
                }
                catch (IOException ioe)
                {
                    WriteMessage(String.Format("Error reparando directorios: {0}", ioe.Message));
                }
            }
            else
            {
                MessageBox.Show("Olvidó seleccionar la carpeta? La carpeta no existe :P");
            }
        }

        private bool StoreMessages()
        {
            if (SaveLog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string savePath = SaveLog.FileName;
                File.WriteAllText(savePath, parsed.Text.Trim());
            }

            return false;
        }

        private string GetFolderName(string path)
        {
            if (Directory.Exists(path))
            {
                int pos = 0,
                    len = path.Length;
                try
                {
                    pos = path.LastIndexOf(Path.DirectorySeparatorChar) + 1;
                    return path.Substring(pos, (len - pos));
                }
                catch (ArgumentException ae)
                {
                    WriteMessage(String.Format("Error getting folder name: {0}", ae.Message));
                }
            }

            return "";
        }

        private void fixFolderNames_Click(object sender, EventArgs e)
        {
            FixFolderNames();
        }

        #region FORM EVENTS
        private void FinderFormatter_Load(object sender, EventArgs e)
        {
            SaveLog.InitialDirectory = ModelSelector.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            FolderBrowser.RootFolder = Environment.SpecialFolder.DesktopDirectory;
            selectedFolder.Text = Path.Combine(Environment.GetFolderPath(FolderBrowser.RootFolder), "Nueva Carpeta");//FolderBrowser.SelectedPath);
            selectedModelFile.Text = Path.Combine(ModelSelector.InitialDirectory, ModelSelector.FileName);
        }

        private void FinderFormatter_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        #endregion

        private void saveMessages_Click(object sender, EventArgs e)
        {
            StoreMessages();
        }

        private void WriteSQLFile()
        {
            WriteMessage("# Writing SQL data #");
            string tmp = magSql.ToString().TrimEnd(",\n".ToCharArray()) + ";\n";
            WriteSQLFile("MagazineSQL.sql",
                SplitSQL(tmp,
                    "USE Finder;\nINSERT INTO FinderSchema.Magazine (PublishYear, MagazineNumber, MagazineName) VALUES\n"));

            tmp = artSql.ToString().TrimEnd(",\n".ToCharArray()) + ";\n";
            //File.WriteAllText(selectedFolder.Text + "\\Art.sql", tmp);
            WriteSQLFile("ArticleSQL.sql",
                SplitSQL(tmp,
                    "USE Finder;\nINSERT INTO FinderSchema.Article (ArticleNumber, ArticleFile, ArticleTitle, ArticleDesc, ArticlePageCount, MagazineID) VALUES\n"));

            tmp = autSql.ToString().TrimEnd("\n".ToCharArray());
            //File.WriteAllText(selectedFolder.Text + "\\Aut.sql", tmp);
            WriteSQLFile("AuthorSQL.sql", SplitSQL(tmp, "USE Finder;\nDECLARE @ID BIGINT\nDECLARE @AID BIGINT\n"));
        }

        private void WriteSQLFile(string filename, List<string> list)
        {
            if (list != null && list.Count > 0)
            {
                int fileCount = 0;
                string ext = Path.GetExtension(filename);
                string name = Path.GetFileNameWithoutExtension(filename);
                string path = selectedFolder.Text;

                foreach (string sqlContent in list)
                {
                    File.WriteAllText(Path.Combine(
                        path,
                        String.Format("{0}{1}{2}", name, fileCount, ext)),
                        sqlContent);
                    fileCount++;
                }
            }
        }

        private List<string> SplitSQL(string content, string prepend = "")
        {
            WriteMessage("# Splitting SQL (990 lines max) #");
            List<string> list = new List<string>();

            if (content.Length > 0)
            {
                char[] sep = { '\n' };
                string[] lines = content.Split(sep);
                WriteMessage("- Content Lines: " + lines.Length);

                if (lines.Length > 1000)
                {
                    StringBuilder sb = new StringBuilder();
                    long totalLines = 0;
                    long blanks = 0;
                    int i = 0;

                    foreach (string line in lines)
                    {
                        if (i >= 990)
                        {
                            if (line == "" || Regex.IsMatch(line, @"^\(.+\),$"))
                            {
                                list.Add(String.Format("{0}{1}", prepend, Regex.Replace(sb.ToString(), @",$", ";")));
                                sb.Clear();
                                totalLines += i;
                                i = 0;
                            }
                        }

                        sb.AppendFormat("{0}\n", line.Trim());
                        i++;

                        if(line == "")
                        {
                            blanks++;
                        }
                    }

                    if (sb.Length > 0)
                        list.Add(String.Format("{0}{1}", prepend, sb.ToString()));

                    sb.Clear();
                    WriteMessage("- Empty Lines: " + blanks);
                    WriteMessage("- Total Lines: " + totalLines);
                }
                else
                {
                    content = prepend + content;
                    list.Add(content);
                }
            }

            return list;
        }

        private void exportSQL_Click(object sender, EventArgs e)
        {
            SaveLog.Filter = "Archivo SQL (*.sql)|*.sql";
            SaveLog.FileName = "FinderInserts";
            SaveLog.Title = "Exportar SQL";
            
            if (SaveLog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                WriteSQLFile();
            }
        }

        private void countPagesBtn_Click(object sender, EventArgs e)
        {
            if (selectedFolder.Text != "")
            {
                CountPagesRecursive(selectedFolder.Text);
            }
            else
            {
                MessageBox.Show("Por favor seleccione una carpeta antes de utilizar esta función. Tenga en cuenta que esta función fallará si la estructura de las carpetas no es correcta (año/número_volumen/número_artículo/archivos.pdf).");
            }
        }

        private void CountPagesRecursive(string baseFolder)
        {
            if (Directory.Exists(baseFolder))
            {
                WriteMessage(String.Format("--- Counting pages in folder {0} ---", baseFolder));
                IEnumerable<string> yearFolders = Directory.EnumerateDirectories(baseFolder);
                StringBuilder skipped = new StringBuilder("Skipped folders:\n\n");
                string nl = "\n";
                PdfReader reader = null;
                
                foreach (string yearFolder in yearFolders)
                {
                    string year = GetFolderName(yearFolder);
                    WriteMessage(String.Format("#----- YEAR {0} -----", year));
                    IEnumerable<string> magNumberFolders = Directory.EnumerateDirectories(yearFolder);
                    foreach (string magNumberFolder in magNumberFolders)
                    {
                        string magNumber = GetFolderName(magNumberFolder);
                        WriteMessage(String.Format("#----- MAG_NUMBER {0} -----#", magNumber));
                        IEnumerable<string> articleNumberFolders = Directory.GetDirectories(magNumberFolder);

                        foreach (string articleNoFolder in articleNumberFolders)
                        {
                            string artNo = GetFolderName(articleNoFolder);
                            WriteMessage(String.Format("#----- ART_NUMBER {0} -----#", artNo));
                            string[] files = Directory.GetFiles(articleNoFolder);

                            if (files.Length > 0)
                            {
                                foreach (string path in files)
                                {
                                    string filename = Path.GetFileName(path);

                                    if (Regex.IsMatch(filename, @"^\d+_\d+_\d+.+\.pdf"))
                                    {
                                        reader = new PdfReader(path);
                                        artSqlUpdate.AppendFormat("-- FOLDER {0}{1}", articleNoFolder, nl);
                                        artSqlUpdate.AppendFormat("UPDATE FinderSchema.Article SET ArticlePageCount = {0} WHERE ArticleFile = '{1}';{2}", reader.NumberOfPages, filename, nl);
                                        reader.Close();
                                        reader = null;
                                    }
                                }
                            }
                        }
                    }
                }

                File.WriteAllText(Path.Combine(baseFolder, "ArticleUpdate.sql"), artSqlUpdate.ToString());
                artSqlUpdate.Clear();
                WriteMessage(skipped.ToString());
                WriteMessage("--- DONE COUNTING PAGES!");
            }
        }

        private void renameFiles_Click(object sender, EventArgs e)
        {
            try
            {
                RenameFiles(selectedFolder.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void RenameFiles(string path, string ext = ".pdf")
        {
            if (Directory.Exists(path))
            {
                WriteMessage("# Renaming Files in path " + path + " #");
                if(!Regex.IsMatch(ext, @"^\.\w+$"))
                    ext = ".pdf";

                string oldfname = "",
                    newfname = "";

                IEnumerable<string> files = Directory.EnumerateFiles(path, "*" + ext, SearchOption.AllDirectories);

                foreach (string filePath in files)
                {
                    WriteMessage("-- File: " + filePath);
                    oldfname = Path.GetFileName(filePath);
                    WriteMessage("--- Old file name: " + oldfname);
                    newfname = RenameFile(oldfname);
                    WriteMessage(String.Format("--- New file name: {0}", newfname));
                    string newPath = Path.Combine(Path.GetDirectoryName(filePath), newfname);
                    WriteMessage("--- New path: " + newPath);
                    File.Move(filePath, newPath);

                    if (!File.Exists(newPath))
                    {
                        WriteMessage("### COULD not move the file! ###");
                    }
                }
            }
        }

        private string RenameFile(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            string newfname = "";

            string[] split = fileName.Split('_');
            long magNo = long.Parse(Regex.Replace(split[0], @"[^\d]+", "").Trim());
            long artNo = long.Parse(Regex.Replace(split[1], @"[^\d]+", "").Trim());

            if (split.Length == 3)
            {
                long fileNo = long.Parse(Regex.Replace(split[2], @"[^\d]+", "").Trim());
                newfname = string.Format("{0}_{1}_{2}{3}", magNo, artNo, fileNo, ext);
            }
            else
            {
                newfname = String.Format("{0}_{1}{2}", magNo, artNo, ext);
            }

            return newfname;
        }

        private string EscapeSingleQuotes(string content)
        {
            WriteMessage("--- Escaping single quotes... ", false);
            string r = Regex.Replace(content, @"(?<!')'(?!')", "''", RegexOptions.Multiline);
            WriteMessage("Done!");
            return r;
        }
    }
}